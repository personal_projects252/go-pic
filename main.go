package main

import (
	"image"
	"image/png"
	"log"
	"os"
	"pic/internal/convolution"
	"pic/internal/matrix"
)

func main() {
	img, err := openImage("image/minon.png")
	if err != nil {
		log.Fatal(err)
	}
	out := convolution.Convolve(img, matrix.UnsharpMasking5x5)
	saveImage(out, "minon_padded.png")
}

func openImage(path string) (image.Image, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	img, err := png.Decode(f)
	if err != nil {
		return nil, err
	}
	return img, nil
}

func saveImage(img image.Image, imgName string) error {
	f, err := os.Create(imgName)
	if err != nil {
		return err
	}
	defer f.Close()
	png.Encode(f, img)
	return nil
}
