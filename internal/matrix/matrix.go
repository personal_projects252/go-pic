package matrix

import (
	"errors"
	"fmt"
	"strings"
)

var ErrShape = errors.New("matrix has incorrect shape")
var ErrDimensions = errors.New("matrix has incorrect dimensions")

type Matrix interface {
	// At returns element at position (x, y).
	At(x, y int) float64
	// MaxX returns horizontal length.
	MaxX() int
	// MaxY returns vertical length.
	MaxY() int
	// Transposed returns transposed matrix of given.
	Transposed() Matrix
}

type Kernel struct {
	Matrix []float64
	Width  int
	Height int
}

var Identity Kernel = Kernel{
	Matrix: []float64{
		0, 0, 0,
		0, 1, 0,
	    0, 0, 0},
	Width: 3,
	Height: 3,
}

var EdgeD Kernel = Kernel{
	Matrix: []float64{
		1, 0,-1,
		0, 0, 0,
	   -1, 0, 1},
	Width: 3,
	Height: 3,
}

var EdgeL Kernel = Kernel{
	Matrix: []float64{
	    0, -1, 0,
       -1,  4,-1,
        0, -1, 0},
	Width: 3,
	Height: 3,
}

var EdgeC Kernel = Kernel{
	Matrix: []float64{
	   -1, -1,-1,
       -1,  8,-1,
       -1, -1,-1},
	Width: 3,
	Height: 3,
}

var Sharpen Kernel = Kernel{
	Matrix: []float64{
	    0, -1, 0,
       -1,  5,-1,
        0, -1, 0},
	Width: 3,
	Height: 3,
}

var BoxBlur Kernel = Kernel{
	Matrix: []float64{
		float64(1)/9, float64(1)/9, float64(1)/9,
		float64(1)/9, float64(1)/9, float64(1)/9,
		float64(1)/9, float64(1)/9, float64(1)/9},
	Width: 3,
	Height: 3,
}

var GaussianBlur3x3 Kernel = Kernel{
	Matrix: []float64{
		float64(1)/16, float64(2)/16, float64(1)/16,
		float64(2)/16, float64(4)/16, float64(2)/16,
		float64(1)/16, float64(2)/16, float64(1)/16},
	Width: 3,
	Height: 3,
}

var GaussianBlur5x5 Kernel = Kernel{
	Matrix: []float64{
		float64(1)/256,float64(4)/256,float64(6)/256,float64(4)/256,float64(1)/256,
		float64(4)/256,float64(16)/256,float64(24)/256,float64(16)/256,float64(4)/256,
		float64(6)/256,float64(24)/256,float64(36)/256,float64(24)/256,float64(6)/256,
		float64(4)/256,float64(16)/256,float64(24)/256,float64(16)/256,float64(4)/256,
		float64(1)/256,float64(4)/256,float64(6)/256,float64(4)/256,float64(1)/256},
	Width: 5,
	Height: 5,
}

var UnsharpMasking5x5 Kernel = Kernel{
	Matrix: []float64{
		-float64(1)/256,-float64(4)/256,-float64(6)/256,-float64(4)/256,-float64(1)/256,
		-float64(4)/256,-float64(16)/256,-float64(24)/256,-float64(16)/256,-float64(4)/256,
		-float64(6)/256,-float64(24)/256,-float64(-476)/256,-float64(24)/256,-float64(6)/256,
		-float64(4)/256,-float64(16)/256,-float64(24)/256,-float64(16)/256,-float64(4)/256,
		-float64(1)/256,-float64(4)/256,-float64(6)/256,-float64(4)/256,-float64(1)/256},
	Width: 5,
	Height: 5,
}

// NewKernel returns kernel.
// NewKernel panic if width or height are zero or less.
func NewKernel(width, height int) Kernel {
	if width <= 0 || height <= 0 {
		panic(ErrDimensions)
	}
	return Kernel{
		Matrix: make([]float64, width*height),
		Width: width,
		Height: height,
	}
}

func (k Kernel) At(x, y int) float64 {
	return k.Matrix[y*k.Width+x]
}

func (k Kernel) MaxX() int {
	return k.Width
}

func (k Kernel) MaxY() int {
	return k.Height
}

func (k Kernel) Transposed() Matrix {
	t := NewKernel(k.Width, k.Height)
	for y := 0; y < k.Height; y++ {
		for x := 0; x < k.Width; x++ {
			t.Matrix[x*k.Height+y] = k.Matrix[y*k.Width+x]
		}
	}
	return t
}

func (k Kernel) String() string {
	var b strings.Builder
	fmt.Fprintf(&b, "[  ")
	for y := 0; y < k.Height; y++ {
		for x := 0; x < k.Width; x++ {
			fmt.Fprintf(&b , "%f ", k.At(x, y))
		}
		if y == k.Height-1 {
			fmt.Fprintf(&b, " ]")
			continue
		}
		fmt.Fprintf(&b, "\n   ")
	}
	return b.String()
}

func Flip(k Kernel) Kernel {
	fk := NewKernel(k.Width, k.Height)
	for y := fk.Height - 1; y >= 0; y-- {
		for x := fk.Width - 1; x >= 0; x-- {
			fk.Matrix[(fk.Height-1-y)*fk.Width + (fk.Width-1-x)] = k.At(x, y)
		}
	}
	return fk
}
