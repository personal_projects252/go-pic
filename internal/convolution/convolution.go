package convolution

import (
	"image"
	"image/draw"
	"math"
	"pic/internal/matrix"
)

type padding int

const (
	wrap padding = iota
	extend
	zero
)

type side int

const (
	up side = iota
	down
	right
	left
)

// Convolve returns result of convolution between in and k.
func Convolve(in image.Image, k matrix.Kernel) *image.RGBA {
	rx := k.Width / 2
	ry := k.Height / 2
	fk := matrix.Flip(k)
	padded := pad(in, rx, ry, extend)
	out := image.NewRGBA(in.Bounds())

	// Pix[(y-Rect.Min.Y)*Stride + (x-Rect.Min.X)*4]
	for y := ry; y < padded.Bounds().Dy() - ry; y++ {
		for x := rx; x < padded.Bounds().Dx() - rx; x++ {
			var r, g, b float64
			for ky := 0; ky < fk.Height; ky++ {
				yy := y - ry + ky
				for kx := 0; kx < fk.Width; kx++ {
					xx := x - rx + kx
					kValue := fk.At(kx, ky)
					pixPos := yy*padded.Stride + xx*4
					r += float64(padded.Pix[pixPos+0]) * kValue
					g += float64(padded.Pix[pixPos+1]) * kValue
					b += float64(padded.Pix[pixPos+2]) * kValue
				}
			}
			pixPos := (y-ry)*out.Stride + (x-rx)*4
			out.Pix[pixPos+0] = uint8(math.Max(math.Min(r, 255), 0))
			out.Pix[pixPos+1] = uint8(math.Max(math.Min(g, 255), 0))
			out.Pix[pixPos+2] = uint8(math.Max(math.Min(b, 255), 0))
			out.Pix[pixPos+3] = padded.Pix[y*padded.Stride+x*4+3]
		}
	}

	return out
}

func pad(in image.Image, rx, ry int, p padding) *image.RGBA {
	var padded *image.RGBA
	switch p {
	case wrap:
		padded = wrapPadding(in, rx, ry)
	case extend:
		padded = extendPadding(in, rx, ry)
	case zero:
		padded = zeroPadding(in, rx, ry)
	default:
		panic("unknown padding mode")
	}
	return padded
}

// wrapPadding pads image with ...
func wrapPadding(in image.Image, rx, ry int) *image.RGBA {
	return nil
}

// extendPadding pads image with pixels on the edges.
func extendPadding(in image.Image, rx, ry int) *image.RGBA {
	out := zeroPadding(in, rx, ry)

	extendSide(out, up, ry)
	extendSide(out, down, ry)
	extendSide(out, right, rx)
	extendSide(out, left, rx)

	return out
}

// extendSide extends image towards side s for el pixels.
func extendSide(img *image.RGBA, s side, el int) {
	switch s {
	case up:
		for i := el; i > 0; i-- {
			sr := image.Rect(0, i, img.Bounds().Dx(), i+1)
			dp := image.Point{0, i - 1}
			dr := image.Rectangle{dp, dp.Add(sr.Size())}
			draw.Draw(img, dr, img, sr.Min, draw.Src)
		}
	case down:
		for i := img.Rect.Dy() - el; i < img.Rect.Dy(); i++ {
			sr := image.Rect(0, i, img.Bounds().Dx(), i-1)
			dp := image.Point{0, i}
			dr := image.Rectangle{dp, dp.Add(sr.Size())}
			draw.Draw(img, dr, img, sr.Min, draw.Src)
		}
	case right:
		for i := img.Rect.Dx() - el; i < img.Rect.Dx(); i++ {
			sr := image.Rect(i-1, 0, i, img.Bounds().Dy())
			dp := image.Point{i, 0}
			dr := image.Rectangle{dp, dp.Add(sr.Size())}
			draw.Draw(img, dr, img, sr.Min, draw.Src)
		}
	case left:
		for i := el; i > 0; i-- {
			sr := image.Rect(i, 0, i+1, img.Bounds().Dy())
			dp := image.Point{i-1, 0}
			dr := image.Rectangle{dp, dp.Add(sr.Size())}
			draw.Draw(img, dr, img, sr.Min, draw.Src)
		}
	default:
		panic("unknown side")
	}
}

// zeroPadding pads image with zeroes.
func zeroPadding(in image.Image, rx, ry int) *image.RGBA {
	paddedW := in.Bounds().Dx() + 2*rx
	paddedH := in.Bounds().Dy() + 2*ry
	newRect := image.Rect(0, 0, paddedW, paddedH)
	fillRect := image.Rect(rx, ry, rx+in.Bounds().Dx(), ry+in.Bounds().Dy())
	out := image.NewRGBA(newRect)
	draw.Draw(out, fillRect, in, in.Bounds().Min, draw.Src)
	return out
}
